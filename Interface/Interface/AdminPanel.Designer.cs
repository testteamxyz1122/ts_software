﻿namespace Interface
{
    partial class AdminPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdminPanel));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.filesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backUpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.restoreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.studentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.feeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bregistration = new System.Windows.Forms.Button();
            this.bfeerecord = new System.Windows.Forms.Button();
            this.bsearch = new System.Windows.Forms.Button();
            this.bbackup = new System.Windows.Forms.Button();
            this.breport = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.Maroon;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.filesToolStripMenuItem,
            this.studentsToolStripMenuItem,
            this.feeToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1314, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "AdminPanel";
            // 
            // filesToolStripMenuItem
            // 
            this.filesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.backUpToolStripMenuItem,
            this.restoreToolStripMenuItem});
            this.filesToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.filesToolStripMenuItem.Name = "filesToolStripMenuItem";
            this.filesToolStripMenuItem.Size = new System.Drawing.Size(42, 20);
            this.filesToolStripMenuItem.Text = "Files";
            // 
            // backUpToolStripMenuItem
            // 
            this.backUpToolStripMenuItem.BackColor = System.Drawing.Color.Maroon;
            this.backUpToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.backUpToolStripMenuItem.Name = "backUpToolStripMenuItem";
            this.backUpToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.backUpToolStripMenuItem.Text = "Back-Up";
            // 
            // restoreToolStripMenuItem
            // 
            this.restoreToolStripMenuItem.BackColor = System.Drawing.Color.Maroon;
            this.restoreToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.restoreToolStripMenuItem.Name = "restoreToolStripMenuItem";
            this.restoreToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.restoreToolStripMenuItem.Text = "Restore";
            // 
            // studentsToolStripMenuItem
            // 
            this.studentsToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.studentsToolStripMenuItem.Name = "studentsToolStripMenuItem";
            this.studentsToolStripMenuItem.Size = new System.Drawing.Size(68, 20);
            this.studentsToolStripMenuItem.Text = "Students ";
            // 
            // feeToolStripMenuItem
            // 
            this.feeToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.feeToolStripMenuItem.Name = "feeToolStripMenuItem";
            this.feeToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.feeToolStripMenuItem.Text = "Fee";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.exitToolStripMenuItem.Text = "Exit";
            // 
            // bregistration
            // 
            this.bregistration.BackColor = System.Drawing.Color.Maroon;
            this.bregistration.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bregistration.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.bregistration.Location = new System.Drawing.Point(1086, 163);
            this.bregistration.Name = "bregistration";
            this.bregistration.Size = new System.Drawing.Size(198, 41);
            this.bregistration.TabIndex = 8;
            this.bregistration.Text = "REGISTRATION";
            this.bregistration.UseVisualStyleBackColor = false;
            // 
            // bfeerecord
            // 
            this.bfeerecord.BackColor = System.Drawing.Color.Maroon;
            this.bfeerecord.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bfeerecord.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.bfeerecord.Location = new System.Drawing.Point(1086, 230);
            this.bfeerecord.Name = "bfeerecord";
            this.bfeerecord.Size = new System.Drawing.Size(198, 41);
            this.bfeerecord.TabIndex = 9;
            this.bfeerecord.Text = "FEE RECORD";
            this.bfeerecord.UseVisualStyleBackColor = false;
            // 
            // bsearch
            // 
            this.bsearch.BackColor = System.Drawing.Color.Maroon;
            this.bsearch.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bsearch.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.bsearch.Location = new System.Drawing.Point(1086, 295);
            this.bsearch.Name = "bsearch";
            this.bsearch.Size = new System.Drawing.Size(198, 41);
            this.bsearch.TabIndex = 10;
            this.bsearch.Text = "SEARCH";
            this.bsearch.UseVisualStyleBackColor = false;
            // 
            // bbackup
            // 
            this.bbackup.BackColor = System.Drawing.Color.Maroon;
            this.bbackup.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bbackup.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.bbackup.Location = new System.Drawing.Point(1086, 366);
            this.bbackup.Name = "bbackup";
            this.bbackup.Size = new System.Drawing.Size(198, 41);
            this.bbackup.TabIndex = 11;
            this.bbackup.Text = "BACK-UP";
            this.bbackup.UseVisualStyleBackColor = false;
            // 
            // breport
            // 
            this.breport.BackColor = System.Drawing.Color.Maroon;
            this.breport.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.breport.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.breport.Location = new System.Drawing.Point(1086, 433);
            this.breport.Name = "breport";
            this.breport.Size = new System.Drawing.Size(198, 41);
            this.breport.TabIndex = 12;
            this.breport.Text = "REPORT";
            this.breport.UseVisualStyleBackColor = false;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Maroon;
            this.label2.ImageAlign = System.Drawing.ContentAlignment.TopRight;
            this.label2.Location = new System.Drawing.Point(641, 327);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(376, 26);
            this.label2.TabIndex = 14;
            this.label2.Text = "Get Knowledge, Spread Knowledge";
            // 
            // AdminPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1314, 613);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.breport);
            this.Controls.Add(this.bbackup);
            this.Controls.Add(this.bsearch);
            this.Controls.Add(this.bfeerecord);
            this.Controls.Add(this.bregistration);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "AdminPanel";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AdminPanel";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem filesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem studentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem feeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.Button bregistration;
        private System.Windows.Forms.Button bfeerecord;
        private System.Windows.Forms.Button bsearch;
        private System.Windows.Forms.Button bbackup;
        private System.Windows.Forms.Button breport;
        private System.Windows.Forms.ToolStripMenuItem backUpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem restoreToolStripMenuItem;
        private System.Windows.Forms.Label label2;
    }
}