﻿namespace TechStep
{
    partial class ViewAll
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewAll));
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.Search_TxtBox = new System.Windows.Forms.TextBox();
            this.BatchNo_Txt = new System.Windows.Forms.TextBox();
            this.btn_View = new System.Windows.Forms.Button();
            this.btn_ViewAll = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.txt_Address1 = new System.Windows.Forms.RichTextBox();
            this.txt_Discount7 = new System.Windows.Forms.TextBox();
            this.txt_Remaining6 = new System.Windows.Forms.TextBox();
            this.txt_Fee5 = new System.Windows.Forms.TextBox();
            this.txt_FName3 = new System.Windows.Forms.TextBox();
            this.txt_Contact2 = new System.Windows.Forms.TextBox();
            this.txt_Name1 = new System.Windows.Forms.TextBox();
            this.btn_Save = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.btn_Cleared = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.label13 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label14 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.lOGO_pictureBox = new System.Windows.Forms.PictureBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel5.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lOGO_pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBox1
            // 
            this.comboBox1.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Course",
            "Name",
            "Student ID"});
            this.comboBox1.Location = new System.Drawing.Point(524, 300);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 2;
            // 
            // Search_TxtBox
            // 
            this.Search_TxtBox.Location = new System.Drawing.Point(682, 301);
            this.Search_TxtBox.Name = "Search_TxtBox";
            this.Search_TxtBox.Size = new System.Drawing.Size(158, 20);
            this.Search_TxtBox.TabIndex = 5;
            // 
            // BatchNo_Txt
            // 
            this.BatchNo_Txt.Location = new System.Drawing.Point(682, 345);
            this.BatchNo_Txt.Name = "BatchNo_Txt";
            this.BatchNo_Txt.Size = new System.Drawing.Size(158, 20);
            this.BatchNo_Txt.TabIndex = 7;
            // 
            // btn_View
            // 
            this.btn_View.BackColor = System.Drawing.Color.Firebrick;
            this.btn_View.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_View.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btn_View.Location = new System.Drawing.Point(875, 336);
            this.btn_View.Name = "btn_View";
            this.btn_View.Size = new System.Drawing.Size(75, 32);
            this.btn_View.TabIndex = 8;
            this.btn_View.Text = "View";
            this.btn_View.UseVisualStyleBackColor = false;
            // 
            // btn_ViewAll
            // 
            this.btn_ViewAll.BackColor = System.Drawing.Color.Firebrick;
            this.btn_ViewAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ViewAll.ForeColor = System.Drawing.Color.White;
            this.btn_ViewAll.Location = new System.Drawing.Point(1044, 336);
            this.btn_ViewAll.Name = "btn_ViewAll";
            this.btn_ViewAll.Size = new System.Drawing.Size(169, 32);
            this.btn_ViewAll.TabIndex = 9;
            this.btn_ViewAll.Text = "View All Students";
            this.btn_ViewAll.UseVisualStyleBackColor = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Modern No. 20", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(399, 18);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(214, 29);
            this.label11.TabIndex = 29;
            this.label11.Text = "View All Students";
            this.label11.Click += new System.EventHandler(this.label11_Click);
            // 
            // txt_Address1
            // 
            this.txt_Address1.Location = new System.Drawing.Point(191, 407);
            this.txt_Address1.Name = "txt_Address1";
            this.txt_Address1.Size = new System.Drawing.Size(158, 46);
            this.txt_Address1.TabIndex = 44;
            this.txt_Address1.Text = "";
            // 
            // txt_Discount7
            // 
            this.txt_Discount7.Location = new System.Drawing.Point(191, 574);
            this.txt_Discount7.Name = "txt_Discount7";
            this.txt_Discount7.Size = new System.Drawing.Size(158, 20);
            this.txt_Discount7.TabIndex = 42;
            // 
            // txt_Remaining6
            // 
            this.txt_Remaining6.Location = new System.Drawing.Point(191, 619);
            this.txt_Remaining6.Name = "txt_Remaining6";
            this.txt_Remaining6.Size = new System.Drawing.Size(158, 20);
            this.txt_Remaining6.TabIndex = 38;
            // 
            // txt_Fee5
            // 
            this.txt_Fee5.Location = new System.Drawing.Point(191, 523);
            this.txt_Fee5.Name = "txt_Fee5";
            this.txt_Fee5.Size = new System.Drawing.Size(158, 20);
            this.txt_Fee5.TabIndex = 36;
            // 
            // txt_FName3
            // 
            this.txt_FName3.Location = new System.Drawing.Point(191, 336);
            this.txt_FName3.Name = "txt_FName3";
            this.txt_FName3.Size = new System.Drawing.Size(158, 20);
            this.txt_FName3.TabIndex = 33;
            // 
            // txt_Contact2
            // 
            this.txt_Contact2.Location = new System.Drawing.Point(191, 381);
            this.txt_Contact2.Name = "txt_Contact2";
            this.txt_Contact2.Size = new System.Drawing.Size(158, 20);
            this.txt_Contact2.TabIndex = 31;
            // 
            // txt_Name1
            // 
            this.txt_Name1.Location = new System.Drawing.Point(191, 291);
            this.txt_Name1.Name = "txt_Name1";
            this.txt_Name1.Size = new System.Drawing.Size(158, 20);
            this.txt_Name1.TabIndex = 29;
            // 
            // btn_Save
            // 
            this.btn_Save.BackColor = System.Drawing.Color.Firebrick;
            this.btn_Save.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Save.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btn_Save.Location = new System.Drawing.Point(274, 656);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(75, 32);
            this.btn_Save.TabIndex = 45;
            this.btn_Save.Text = "Save";
            this.btn_Save.UseVisualStyleBackColor = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Maroon;
            this.label12.Location = new System.Drawing.Point(439, 300);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(79, 16);
            this.label12.TabIndex = 46;
            this.label12.Text = "Search By";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Maroon;
            this.label2.Location = new System.Drawing.Point(585, 346);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 16);
            this.label2.TabIndex = 47;
            this.label2.Text = "Batch No.";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Maroon;
            this.label3.Location = new System.Drawing.Point(91, 291);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 16);
            this.label3.TabIndex = 48;
            this.label3.Text = "Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Maroon;
            this.label1.Location = new System.Drawing.Point(88, 336);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 16);
            this.label1.TabIndex = 49;
            this.label1.Text = "Father Name";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Maroon;
            this.label5.Location = new System.Drawing.Point(20, 386);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(0, 16);
            this.label5.TabIndex = 50;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Maroon;
            this.label4.Location = new System.Drawing.Point(91, 382);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 16);
            this.label4.TabIndex = 51;
            this.label4.Text = "Contact";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Maroon;
            this.label6.Location = new System.Drawing.Point(91, 426);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 16);
            this.label6.TabIndex = 52;
            this.label6.Text = "Address";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Maroon;
            this.label10.Location = new System.Drawing.Point(91, 480);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(59, 16);
            this.label10.TabIndex = 53;
            this.label10.Text = "Gender";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Maroon;
            this.label7.Location = new System.Drawing.Point(90, 531);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 16);
            this.label7.TabIndex = 54;
            this.label7.Text = "Fee";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Maroon;
            this.label9.Location = new System.Drawing.Point(90, 579);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(68, 16);
            this.label9.TabIndex = 55;
            this.label9.Text = "Discount";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Maroon;
            this.label8.Location = new System.Drawing.Point(91, 627);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(82, 16);
            this.label8.TabIndex = 56;
            this.label8.Text = "Remaining";
            // 
            // btn_Cleared
            // 
            this.btn_Cleared.AutoSize = true;
            this.btn_Cleared.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Cleared.ForeColor = System.Drawing.Color.Maroon;
            this.btn_Cleared.Location = new System.Drawing.Point(191, 476);
            this.btn_Cleared.Name = "btn_Cleared";
            this.btn_Cleared.Size = new System.Drawing.Size(75, 22);
            this.btn_Cleared.TabIndex = 57;
            this.btn_Cleared.TabStop = true;
            this.btn_Cleared.Text = "Female";
            this.btn_Cleared.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton1.ForeColor = System.Drawing.Color.Maroon;
            this.radioButton1.Location = new System.Drawing.Point(283, 476);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(58, 22);
            this.radioButton1.TabIndex = 58;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Male";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Times New Roman", 21.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Firebrick;
            this.label13.Location = new System.Drawing.Point(493, -1);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(443, 33);
            this.label13.TabIndex = 59;
            this.label13.Text = "Get Knowledge , Spread Knowledge";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(421, 407);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(788, 301);
            this.dataGridView1.TabIndex = 31;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Times New Roman", 80.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label14.Location = new System.Drawing.Point(228, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(601, 119);
            this.label14.TabIndex = 0;
            this.label14.Text = "TECHSTEP";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Firebrick;
            this.panel5.Controls.Add(this.label14);
            this.panel5.Location = new System.Drawing.Point(228, 49);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1133, 128);
            this.panel5.TabIndex = 63;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Firebrick;
            this.panel1.Location = new System.Drawing.Point(2, 49);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(27, 141);
            this.panel1.TabIndex = 61;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Firebrick;
            this.panel2.Controls.Add(this.label11);
            this.panel2.Location = new System.Drawing.Point(186, 196);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(926, 47);
            this.panel2.TabIndex = 62;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Firebrick;
            this.panel3.Location = new System.Drawing.Point(2, 196);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(138, 48);
            this.panel3.TabIndex = 64;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Firebrick;
            this.panel4.Location = new System.Drawing.Point(149, 196);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(12, 48);
            this.panel4.TabIndex = 66;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Firebrick;
            this.panel6.Location = new System.Drawing.Point(167, 196);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(13, 48);
            this.panel6.TabIndex = 67;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.Firebrick;
            this.panel7.Location = new System.Drawing.Point(1118, 195);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(13, 48);
            this.panel7.TabIndex = 68;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.Firebrick;
            this.panel8.Location = new System.Drawing.Point(1137, 195);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(13, 48);
            this.panel8.TabIndex = 69;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.Firebrick;
            this.panel9.Location = new System.Drawing.Point(1156, 195);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(205, 48);
            this.panel9.TabIndex = 65;
            // 
            // lOGO_pictureBox
            // 
            this.lOGO_pictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("lOGO_pictureBox.BackgroundImage")));
            this.lOGO_pictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.lOGO_pictureBox.Location = new System.Drawing.Point(58, 49);
            this.lOGO_pictureBox.Name = "lOGO_pictureBox";
            this.lOGO_pictureBox.Size = new System.Drawing.Size(137, 89);
            this.lOGO_pictureBox.TabIndex = 70;
            this.lOGO_pictureBox.TabStop = false;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Times New Roman", 24F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label16.Location = new System.Drawing.Point(62, 141);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(78, 36);
            this.label16.TabIndex = 71;
            this.label16.Text = "Tech";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Times New Roman", 24F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label15.Location = new System.Drawing.Point(132, 141);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(72, 36);
            this.label15.TabIndex = 72;
            this.label15.Text = "Step";
            // 
            // ViewAll
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(1360, 749);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.lOGO_pictureBox);
            this.Controls.Add(this.panel9);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.radioButton1);
            this.Controls.Add(this.btn_Cleared);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.btn_Save);
            this.Controls.Add(this.txt_Discount7);
            this.Controls.Add(this.txt_Address1);
            this.Controls.Add(this.txt_Remaining6);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.txt_Fee5);
            this.Controls.Add(this.btn_ViewAll);
            this.Controls.Add(this.btn_View);
            this.Controls.Add(this.BatchNo_Txt);
            this.Controls.Add(this.Search_TxtBox);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.txt_FName3);
            this.Controls.Add(this.txt_Contact2);
            this.Controls.Add(this.txt_Name1);
            this.Name = "ViewAll";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Address";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ViewAll_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lOGO_pictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox Search_TxtBox;
        private System.Windows.Forms.TextBox BatchNo_Txt;
        private System.Windows.Forms.Button btn_View;
        private System.Windows.Forms.Button btn_ViewAll;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.RichTextBox txt_Address1;
        private System.Windows.Forms.TextBox txt_Discount7;
        private System.Windows.Forms.TextBox txt_Remaining6;
        private System.Windows.Forms.TextBox txt_Fee5;
        private System.Windows.Forms.TextBox txt_FName3;
        private System.Windows.Forms.TextBox txt_Contact2;
        private System.Windows.Forms.TextBox txt_Name1;
        private System.Windows.Forms.Button btn_Save;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.RadioButton btn_Cleared;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.PictureBox lOGO_pictureBox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
    }
}

