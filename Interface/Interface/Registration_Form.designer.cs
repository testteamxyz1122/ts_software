﻿namespace _1st_project_TechStep
{
    partial class Registation_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Registation_Form));
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.Course_Label = new System.Windows.Forms.Label();
            this.Course_Select_ComboBox = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.Name_Label = new System.Windows.Forms.Label();
            this.Name_TextBox = new System.Windows.Forms.TextBox();
            this.FatherName_label = new System.Windows.Forms.Label();
            this.F_Name_TextBox = new System.Windows.Forms.TextBox();
            this.CNIC_Label = new System.Windows.Forms.Label();
            this.Contact_Label = new System.Windows.Forms.Label();
            this.Contact_textBox = new System.Windows.Forms.TextBox();
            this.Adress_label = new System.Windows.Forms.Label();
            this.Adress_TextBox = new System.Windows.Forms.TextBox();
            this.Gender_label = new System.Windows.Forms.Label();
            this.Male_Radio_BTN = new System.Windows.Forms.RadioButton();
            this.Female_Radio_BTN = new System.Windows.Forms.RadioButton();
            this.Institute_Label = new System.Windows.Forms.Label();
            this.Institute_textBox = new System.Windows.Forms.TextBox();
            this.Batch_No_Label = new System.Windows.Forms.Label();
            this.Batch_no_TectBox = new System.Windows.Forms.TextBox();
            this.DBO_Label = new System.Windows.Forms.Label();
            this.DBO_dateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.CNIC_maskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.lOGO_pictureBox = new System.Windows.Forms.PictureBox();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.STD_No_Label = new System.Windows.Forms.Label();
            this.STD_NO_TextBox = new System.Windows.Forms.TextBox();
            this.Join_Date_Label = new System.Windows.Forms.Label();
            this.JoinDate_Textbox = new System.Windows.Forms.TextBox();
            this.FeeInfo_label = new System.Windows.Forms.Label();
            this.Discount_checkBox = new System.Windows.Forms.CheckBox();
            this.Discount_Amount_Label = new System.Windows.Forms.Label();
            this.Discount_Amount_textBox = new System.Windows.Forms.TextBox();
            this.Paid_Amount_label = new System.Windows.Forms.Label();
            this.Paid_Amount_textBox = new System.Windows.Forms.TextBox();
            this.ToBePaid_textBox = new System.Windows.Forms.TextBox();
            this.To_Be_Paid_label = new System.Windows.Forms.Label();
            this.Course_Fee_label = new System.Windows.Forms.Label();
            this.Course_Fee_textBox = new System.Windows.Forms.TextBox();
            this.Save_button = new System.Windows.Forms.Button();
            this.Back_button = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.lOGO_pictureBox)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 21.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Firebrick;
            this.label1.Location = new System.Drawing.Point(391, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(443, 33);
            this.label1.TabIndex = 1;
            this.label1.Text = "Get Knowledge , Spread Knowledge";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Firebrick;
            this.panel1.Location = new System.Drawing.Point(10, 67);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(27, 141);
            this.panel1.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Firebrick;
            this.label4.Cursor = System.Windows.Forms.Cursors.PanSE;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label4.Location = new System.Drawing.Point(426, 278);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(208, 31);
            this.label4.TabIndex = 1;
            this.label4.Text = "Course Info.   >>";
            // 
            // Course_Label
            // 
            this.Course_Label.AutoSize = true;
            this.Course_Label.Font = new System.Drawing.Font("Times New Roman", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Course_Label.ForeColor = System.Drawing.Color.Maroon;
            this.Course_Label.Location = new System.Drawing.Point(448, 334);
            this.Course_Label.Name = "Course_Label";
            this.Course_Label.Size = new System.Drawing.Size(70, 22);
            this.Course_Label.TabIndex = 10;
            this.Course_Label.Text = "Course ";
            // 
            // Course_Select_ComboBox
            // 
            this.Course_Select_ComboBox.Font = new System.Drawing.Font("Times New Roman", 6.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Course_Select_ComboBox.FormattingEnabled = true;
            this.Course_Select_ComboBox.Location = new System.Drawing.Point(573, 337);
            this.Course_Select_ComboBox.Name = "Course_Select_ComboBox";
            this.Course_Select_ComboBox.Size = new System.Drawing.Size(160, 19);
            this.Course_Select_ComboBox.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Firebrick;
            this.label5.Cursor = System.Windows.Forms.Cursors.PanSE;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label5.Location = new System.Drawing.Point(4, 278);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(229, 31);
            this.label5.TabIndex = 12;
            this.label5.Text = "Personal Info.   >>";
            // 
            // Name_Label
            // 
            this.Name_Label.AutoSize = true;
            this.Name_Label.Font = new System.Drawing.Font("Times New Roman", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name_Label.ForeColor = System.Drawing.Color.Maroon;
            this.Name_Label.Location = new System.Drawing.Point(47, 337);
            this.Name_Label.Name = "Name_Label";
            this.Name_Label.Size = new System.Drawing.Size(57, 22);
            this.Name_Label.TabIndex = 13;
            this.Name_Label.Text = "Name";
            // 
            // Name_TextBox
            // 
            this.Name_TextBox.Location = new System.Drawing.Point(164, 337);
            this.Name_TextBox.Name = "Name_TextBox";
            this.Name_TextBox.Size = new System.Drawing.Size(160, 20);
            this.Name_TextBox.TabIndex = 14;
            // 
            // FatherName_label
            // 
            this.FatherName_label.AutoSize = true;
            this.FatherName_label.Font = new System.Drawing.Font("Times New Roman", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FatherName_label.ForeColor = System.Drawing.Color.Maroon;
            this.FatherName_label.Location = new System.Drawing.Point(47, 368);
            this.FatherName_label.Name = "FatherName_label";
            this.FatherName_label.Size = new System.Drawing.Size(73, 22);
            this.FatherName_label.TabIndex = 15;
            this.FatherName_label.Text = "F.Name";
            // 
            // F_Name_TextBox
            // 
            this.F_Name_TextBox.Location = new System.Drawing.Point(164, 370);
            this.F_Name_TextBox.Name = "F_Name_TextBox";
            this.F_Name_TextBox.Size = new System.Drawing.Size(160, 20);
            this.F_Name_TextBox.TabIndex = 16;
            // 
            // CNIC_Label
            // 
            this.CNIC_Label.AutoSize = true;
            this.CNIC_Label.Font = new System.Drawing.Font("Times New Roman", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CNIC_Label.ForeColor = System.Drawing.Color.Maroon;
            this.CNIC_Label.Location = new System.Drawing.Point(47, 405);
            this.CNIC_Label.Name = "CNIC_Label";
            this.CNIC_Label.Size = new System.Drawing.Size(56, 22);
            this.CNIC_Label.TabIndex = 17;
            this.CNIC_Label.Text = "CNIC";
            // 
            // Contact_Label
            // 
            this.Contact_Label.AutoSize = true;
            this.Contact_Label.Font = new System.Drawing.Font("Times New Roman", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Contact_Label.ForeColor = System.Drawing.Color.Maroon;
            this.Contact_Label.Location = new System.Drawing.Point(47, 438);
            this.Contact_Label.Name = "Contact_Label";
            this.Contact_Label.Size = new System.Drawing.Size(71, 22);
            this.Contact_Label.TabIndex = 19;
            this.Contact_Label.Text = "Contact";
            // 
            // Contact_textBox
            // 
            this.Contact_textBox.Location = new System.Drawing.Point(164, 440);
            this.Contact_textBox.Name = "Contact_textBox";
            this.Contact_textBox.Size = new System.Drawing.Size(160, 20);
            this.Contact_textBox.TabIndex = 20;
            // 
            // Adress_label
            // 
            this.Adress_label.AutoSize = true;
            this.Adress_label.Font = new System.Drawing.Font("Times New Roman", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Adress_label.ForeColor = System.Drawing.Color.Maroon;
            this.Adress_label.Location = new System.Drawing.Point(47, 513);
            this.Adress_label.Name = "Adress_label";
            this.Adress_label.Size = new System.Drawing.Size(72, 22);
            this.Adress_label.TabIndex = 21;
            this.Adress_label.Text = "Address";
            // 
            // Adress_TextBox
            // 
            this.Adress_TextBox.Location = new System.Drawing.Point(164, 515);
            this.Adress_TextBox.Name = "Adress_TextBox";
            this.Adress_TextBox.Size = new System.Drawing.Size(160, 20);
            this.Adress_TextBox.TabIndex = 22;
            // 
            // Gender_label
            // 
            this.Gender_label.AutoSize = true;
            this.Gender_label.Font = new System.Drawing.Font("Times New Roman", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Gender_label.ForeColor = System.Drawing.Color.Maroon;
            this.Gender_label.Location = new System.Drawing.Point(47, 553);
            this.Gender_label.Name = "Gender_label";
            this.Gender_label.Size = new System.Drawing.Size(68, 22);
            this.Gender_label.TabIndex = 23;
            this.Gender_label.Text = "Gender";
            // 
            // Male_Radio_BTN
            // 
            this.Male_Radio_BTN.AutoSize = true;
            this.Male_Radio_BTN.Font = new System.Drawing.Font("Times New Roman", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Male_Radio_BTN.ForeColor = System.Drawing.Color.DarkRed;
            this.Male_Radio_BTN.Location = new System.Drawing.Point(174, 553);
            this.Male_Radio_BTN.Name = "Male_Radio_BTN";
            this.Male_Radio_BTN.Size = new System.Drawing.Size(58, 22);
            this.Male_Radio_BTN.TabIndex = 24;
            this.Male_Radio_BTN.TabStop = true;
            this.Male_Radio_BTN.Text = "Male";
            this.Male_Radio_BTN.UseVisualStyleBackColor = true;
            // 
            // Female_Radio_BTN
            // 
            this.Female_Radio_BTN.AutoSize = true;
            this.Female_Radio_BTN.Font = new System.Drawing.Font("Times New Roman", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Female_Radio_BTN.ForeColor = System.Drawing.Color.DarkRed;
            this.Female_Radio_BTN.Location = new System.Drawing.Point(250, 553);
            this.Female_Radio_BTN.Name = "Female_Radio_BTN";
            this.Female_Radio_BTN.Size = new System.Drawing.Size(74, 22);
            this.Female_Radio_BTN.TabIndex = 25;
            this.Female_Radio_BTN.TabStop = true;
            this.Female_Radio_BTN.Text = "Female";
            this.Female_Radio_BTN.UseVisualStyleBackColor = true;
            // 
            // Institute_Label
            // 
            this.Institute_Label.AutoSize = true;
            this.Institute_Label.Font = new System.Drawing.Font("Times New Roman", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Institute_Label.ForeColor = System.Drawing.Color.Maroon;
            this.Institute_Label.Location = new System.Drawing.Point(47, 589);
            this.Institute_Label.Name = "Institute_Label";
            this.Institute_Label.Size = new System.Drawing.Size(75, 22);
            this.Institute_Label.TabIndex = 26;
            this.Institute_Label.Text = "Institute";
            // 
            // Institute_textBox
            // 
            this.Institute_textBox.Location = new System.Drawing.Point(164, 591);
            this.Institute_textBox.Name = "Institute_textBox";
            this.Institute_textBox.Size = new System.Drawing.Size(160, 20);
            this.Institute_textBox.TabIndex = 27;
            // 
            // Batch_No_Label
            // 
            this.Batch_No_Label.AutoSize = true;
            this.Batch_No_Label.Font = new System.Drawing.Font("Times New Roman", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Batch_No_Label.ForeColor = System.Drawing.Color.Maroon;
            this.Batch_No_Label.Location = new System.Drawing.Point(448, 379);
            this.Batch_No_Label.Name = "Batch_No_Label";
            this.Batch_No_Label.Size = new System.Drawing.Size(93, 22);
            this.Batch_No_Label.TabIndex = 28;
            this.Batch_No_Label.Text = " Batch No.";
            // 
            // Batch_no_TectBox
            // 
            this.Batch_no_TectBox.Location = new System.Drawing.Point(573, 379);
            this.Batch_no_TectBox.Name = "Batch_no_TectBox";
            this.Batch_no_TectBox.Size = new System.Drawing.Size(160, 20);
            this.Batch_no_TectBox.TabIndex = 29;
            // 
            // DBO_Label
            // 
            this.DBO_Label.AutoSize = true;
            this.DBO_Label.Font = new System.Drawing.Font("Times New Roman", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DBO_Label.ForeColor = System.Drawing.Color.Maroon;
            this.DBO_Label.Location = new System.Drawing.Point(47, 475);
            this.DBO_Label.Name = "DBO_Label";
            this.DBO_Label.Size = new System.Drawing.Size(50, 22);
            this.DBO_Label.TabIndex = 31;
            this.DBO_Label.Text = "DOB";
            // 
            // DBO_dateTimePicker
            // 
            this.DBO_dateTimePicker.CalendarForeColor = System.Drawing.Color.Maroon;
            this.DBO_dateTimePicker.CalendarTitleForeColor = System.Drawing.Color.DarkRed;
            this.DBO_dateTimePicker.Location = new System.Drawing.Point(164, 477);
            this.DBO_dateTimePicker.Name = "DBO_dateTimePicker";
            this.DBO_dateTimePicker.Size = new System.Drawing.Size(160, 20);
            this.DBO_dateTimePicker.TabIndex = 32;
            // 
            // CNIC_maskedTextBox
            // 
            this.CNIC_maskedTextBox.Location = new System.Drawing.Point(164, 407);
            this.CNIC_maskedTextBox.Mask = "000-00-0000";
            this.CNIC_maskedTextBox.Name = "CNIC_maskedTextBox";
            this.CNIC_maskedTextBox.Size = new System.Drawing.Size(160, 20);
            this.CNIC_maskedTextBox.TabIndex = 33;
            // 
            // lOGO_pictureBox
            // 
            this.lOGO_pictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("lOGO_pictureBox.BackgroundImage")));
            this.lOGO_pictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.lOGO_pictureBox.Location = new System.Drawing.Point(67, 67);
            this.lOGO_pictureBox.Name = "lOGO_pictureBox";
            this.lOGO_pictureBox.Size = new System.Drawing.Size(128, 89);
            this.lOGO_pictureBox.TabIndex = 34;
            this.lOGO_pictureBox.TabStop = false;
            this.lOGO_pictureBox.Click += new System.EventHandler(this.lOGO_pictureBox_Click);
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.Firebrick;
            this.panel8.Location = new System.Drawing.Point(1104, 214);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(13, 48);
            this.panel8.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label3.Location = new System.Drawing.Point(355, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(213, 22);
            this.label3.TabIndex = 0;
            this.label3.Text = "REGISTRATION FORM";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Firebrick;
            this.panel2.Controls.Add(this.label3);
            this.panel2.Location = new System.Drawing.Point(165, 214);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(914, 48);
            this.panel2.TabIndex = 4;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Firebrick;
            this.panel3.Location = new System.Drawing.Point(10, 214);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(112, 48);
            this.panel3.TabIndex = 5;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Firebrick;
            this.panel4.Location = new System.Drawing.Point(128, 213);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(12, 48);
            this.panel4.TabIndex = 6;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Firebrick;
            this.panel6.Location = new System.Drawing.Point(146, 214);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(13, 48);
            this.panel6.TabIndex = 7;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.Firebrick;
            this.panel7.Location = new System.Drawing.Point(1085, 213);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(13, 48);
            this.panel7.TabIndex = 8;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.Firebrick;
            this.panel9.Location = new System.Drawing.Point(1123, 213);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(163, 48);
            this.panel9.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 24F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label2.Location = new System.Drawing.Point(70, 159);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 36);
            this.label2.TabIndex = 35;
            this.label2.Text = "Tech";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Times New Roman", 24F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label6.Location = new System.Drawing.Point(140, 159);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 36);
            this.label6.TabIndex = 36;
            this.label6.Text = "Step";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Firebrick;
            this.panel5.Controls.Add(this.label7);
            this.panel5.Location = new System.Drawing.Point(236, 67);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1050, 128);
            this.panel5.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Times New Roman", 80.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label7.Location = new System.Drawing.Point(176, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(601, 119);
            this.label7.TabIndex = 0;
            this.label7.Text = "TECHSTEP";
            // 
            // STD_No_Label
            // 
            this.STD_No_Label.AutoSize = true;
            this.STD_No_Label.Font = new System.Drawing.Font("Times New Roman", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.STD_No_Label.ForeColor = System.Drawing.Color.Maroon;
            this.STD_No_Label.Location = new System.Drawing.Point(448, 424);
            this.STD_No_Label.Name = "STD_No_Label";
            this.STD_No_Label.Size = new System.Drawing.Size(78, 22);
            this.STD_No_Label.TabIndex = 37;
            this.STD_No_Label.Text = "STD No.";
            // 
            // STD_NO_TextBox
            // 
            this.STD_NO_TextBox.Location = new System.Drawing.Point(573, 424);
            this.STD_NO_TextBox.Name = "STD_NO_TextBox";
            this.STD_NO_TextBox.Size = new System.Drawing.Size(160, 20);
            this.STD_NO_TextBox.TabIndex = 38;
            // 
            // Join_Date_Label
            // 
            this.Join_Date_Label.AutoSize = true;
            this.Join_Date_Label.Font = new System.Drawing.Font("Times New Roman", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Join_Date_Label.ForeColor = System.Drawing.Color.Maroon;
            this.Join_Date_Label.Location = new System.Drawing.Point(448, 471);
            this.Join_Date_Label.Name = "Join_Date_Label";
            this.Join_Date_Label.Size = new System.Drawing.Size(87, 22);
            this.Join_Date_Label.TabIndex = 39;
            this.Join_Date_Label.Text = "Join Date";
            // 
            // JoinDate_Textbox
            // 
            this.JoinDate_Textbox.Location = new System.Drawing.Point(573, 473);
            this.JoinDate_Textbox.Name = "JoinDate_Textbox";
            this.JoinDate_Textbox.Size = new System.Drawing.Size(160, 20);
            this.JoinDate_Textbox.TabIndex = 40;
            // 
            // FeeInfo_label
            // 
            this.FeeInfo_label.AutoSize = true;
            this.FeeInfo_label.BackColor = System.Drawing.Color.Firebrick;
            this.FeeInfo_label.Cursor = System.Windows.Forms.Cursors.PanSE;
            this.FeeInfo_label.Font = new System.Drawing.Font("Times New Roman", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FeeInfo_label.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.FeeInfo_label.Location = new System.Drawing.Point(861, 278);
            this.FeeInfo_label.Name = "FeeInfo_label";
            this.FeeInfo_label.Size = new System.Drawing.Size(169, 31);
            this.FeeInfo_label.TabIndex = 41;
            this.FeeInfo_label.Text = "Fee Info.   >>";
            // 
            // Discount_checkBox
            // 
            this.Discount_checkBox.AutoSize = true;
            this.Discount_checkBox.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Discount_checkBox.ForeColor = System.Drawing.Color.Maroon;
            this.Discount_checkBox.Location = new System.Drawing.Point(1060, 370);
            this.Discount_checkBox.Name = "Discount_checkBox";
            this.Discount_checkBox.Size = new System.Drawing.Size(89, 23);
            this.Discount_checkBox.TabIndex = 42;
            this.Discount_checkBox.Text = "Discount";
            this.Discount_checkBox.UseVisualStyleBackColor = true;
            // 
            // Discount_Amount_Label
            // 
            this.Discount_Amount_Label.AutoSize = true;
            this.Discount_Amount_Label.Font = new System.Drawing.Font("Times New Roman", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Discount_Amount_Label.ForeColor = System.Drawing.Color.Maroon;
            this.Discount_Amount_Label.Location = new System.Drawing.Point(876, 405);
            this.Discount_Amount_Label.Name = "Discount_Amount_Label";
            this.Discount_Amount_Label.Size = new System.Drawing.Size(149, 22);
            this.Discount_Amount_Label.TabIndex = 43;
            this.Discount_Amount_Label.Text = "Discount Amount";
            // 
            // Discount_Amount_textBox
            // 
            this.Discount_Amount_textBox.Location = new System.Drawing.Point(1036, 407);
            this.Discount_Amount_textBox.Name = "Discount_Amount_textBox";
            this.Discount_Amount_textBox.Size = new System.Drawing.Size(160, 20);
            this.Discount_Amount_textBox.TabIndex = 44;
            // 
            // Paid_Amount_label
            // 
            this.Paid_Amount_label.AutoSize = true;
            this.Paid_Amount_label.Font = new System.Drawing.Font("Times New Roman", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Paid_Amount_label.ForeColor = System.Drawing.Color.Maroon;
            this.Paid_Amount_label.Location = new System.Drawing.Point(876, 451);
            this.Paid_Amount_label.Name = "Paid_Amount_label";
            this.Paid_Amount_label.Size = new System.Drawing.Size(117, 22);
            this.Paid_Amount_label.TabIndex = 45;
            this.Paid_Amount_label.Text = "Paid Amount";
            // 
            // Paid_Amount_textBox
            // 
            this.Paid_Amount_textBox.Location = new System.Drawing.Point(1036, 451);
            this.Paid_Amount_textBox.Name = "Paid_Amount_textBox";
            this.Paid_Amount_textBox.Size = new System.Drawing.Size(160, 20);
            this.Paid_Amount_textBox.TabIndex = 46;
            // 
            // ToBePaid_textBox
            // 
            this.ToBePaid_textBox.Location = new System.Drawing.Point(1036, 497);
            this.ToBePaid_textBox.Name = "ToBePaid_textBox";
            this.ToBePaid_textBox.Size = new System.Drawing.Size(160, 20);
            this.ToBePaid_textBox.TabIndex = 47;
            // 
            // To_Be_Paid_label
            // 
            this.To_Be_Paid_label.AutoSize = true;
            this.To_Be_Paid_label.Font = new System.Drawing.Font("Times New Roman", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.To_Be_Paid_label.ForeColor = System.Drawing.Color.Maroon;
            this.To_Be_Paid_label.Location = new System.Drawing.Point(876, 495);
            this.To_Be_Paid_label.Name = "To_Be_Paid_label";
            this.To_Be_Paid_label.Size = new System.Drawing.Size(96, 22);
            this.To_Be_Paid_label.TabIndex = 48;
            this.To_Be_Paid_label.Text = "To Be paid";
            // 
            // Course_Fee_label
            // 
            this.Course_Fee_label.AutoSize = true;
            this.Course_Fee_label.Font = new System.Drawing.Font("Times New Roman", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Course_Fee_label.ForeColor = System.Drawing.Color.Maroon;
            this.Course_Fee_label.Location = new System.Drawing.Point(876, 334);
            this.Course_Fee_label.Name = "Course_Fee_label";
            this.Course_Fee_label.Size = new System.Drawing.Size(99, 22);
            this.Course_Fee_label.TabIndex = 49;
            this.Course_Fee_label.Text = "Course Fee";
            // 
            // Course_Fee_textBox
            // 
            this.Course_Fee_textBox.Location = new System.Drawing.Point(1036, 336);
            this.Course_Fee_textBox.Name = "Course_Fee_textBox";
            this.Course_Fee_textBox.Size = new System.Drawing.Size(160, 20);
            this.Course_Fee_textBox.TabIndex = 50;
            // 
            // Save_button
            // 
            this.Save_button.BackColor = System.Drawing.Color.Firebrick;
            this.Save_button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Save_button.Font = new System.Drawing.Font("Times New Roman", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Save_button.ForeColor = System.Drawing.Color.White;
            this.Save_button.Location = new System.Drawing.Point(573, 653);
            this.Save_button.Name = "Save_button";
            this.Save_button.Size = new System.Drawing.Size(120, 42);
            this.Save_button.TabIndex = 51;
            this.Save_button.Text = "Save";
            this.Save_button.UseVisualStyleBackColor = false;
            // 
            // Back_button
            // 
            this.Back_button.BackColor = System.Drawing.Color.Firebrick;
            this.Back_button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Back_button.Font = new System.Drawing.Font("Times New Roman", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Back_button.ForeColor = System.Drawing.Color.White;
            this.Back_button.Location = new System.Drawing.Point(1158, 660);
            this.Back_button.Name = "Back_button";
            this.Back_button.Size = new System.Drawing.Size(120, 42);
            this.Back_button.TabIndex = 52;
            this.Back_button.Text = "Back";
            this.Back_button.UseVisualStyleBackColor = false;
            // 
            // Registation_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1290, 714);
            this.Controls.Add(this.Back_button);
            this.Controls.Add(this.Save_button);
            this.Controls.Add(this.Course_Fee_textBox);
            this.Controls.Add(this.Course_Fee_label);
            this.Controls.Add(this.To_Be_Paid_label);
            this.Controls.Add(this.ToBePaid_textBox);
            this.Controls.Add(this.Paid_Amount_textBox);
            this.Controls.Add(this.Paid_Amount_label);
            this.Controls.Add(this.Discount_Amount_textBox);
            this.Controls.Add(this.Discount_Amount_Label);
            this.Controls.Add(this.Discount_checkBox);
            this.Controls.Add(this.FeeInfo_label);
            this.Controls.Add(this.JoinDate_Textbox);
            this.Controls.Add(this.Join_Date_Label);
            this.Controls.Add(this.STD_NO_TextBox);
            this.Controls.Add(this.STD_No_Label);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lOGO_pictureBox);
            this.Controls.Add(this.CNIC_maskedTextBox);
            this.Controls.Add(this.DBO_dateTimePicker);
            this.Controls.Add(this.DBO_Label);
            this.Controls.Add(this.Batch_no_TectBox);
            this.Controls.Add(this.Batch_No_Label);
            this.Controls.Add(this.Institute_textBox);
            this.Controls.Add(this.Institute_Label);
            this.Controls.Add(this.Female_Radio_BTN);
            this.Controls.Add(this.Male_Radio_BTN);
            this.Controls.Add(this.Gender_label);
            this.Controls.Add(this.Adress_TextBox);
            this.Controls.Add(this.Adress_label);
            this.Controls.Add(this.Contact_textBox);
            this.Controls.Add(this.Contact_Label);
            this.Controls.Add(this.CNIC_Label);
            this.Controls.Add(this.F_Name_TextBox);
            this.Controls.Add(this.FatherName_label);
            this.Controls.Add(this.Name_TextBox);
            this.Controls.Add(this.Name_Label);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.Course_Select_ComboBox);
            this.Controls.Add(this.Course_Label);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.panel9);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Times New Roman", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "Registation_Form";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Registration Form";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.lOGO_pictureBox)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label Course_Label;
        private System.Windows.Forms.ComboBox Course_Select_ComboBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label Name_Label;
        private System.Windows.Forms.TextBox Name_TextBox;
        private System.Windows.Forms.Label FatherName_label;
        private System.Windows.Forms.TextBox F_Name_TextBox;
        private System.Windows.Forms.Label CNIC_Label;
        private System.Windows.Forms.Label Contact_Label;
        private System.Windows.Forms.TextBox Contact_textBox;
        private System.Windows.Forms.Label Adress_label;
        private System.Windows.Forms.TextBox Adress_TextBox;
        private System.Windows.Forms.Label Gender_label;
        private System.Windows.Forms.RadioButton Male_Radio_BTN;
        private System.Windows.Forms.RadioButton Female_Radio_BTN;
        private System.Windows.Forms.Label Institute_Label;
        private System.Windows.Forms.TextBox Institute_textBox;
        private System.Windows.Forms.Label Batch_No_Label;
        private System.Windows.Forms.TextBox Batch_no_TectBox;
        private System.Windows.Forms.Label DBO_Label;
        private System.Windows.Forms.DateTimePicker DBO_dateTimePicker;
        private System.Windows.Forms.MaskedTextBox CNIC_maskedTextBox;
        private System.Windows.Forms.PictureBox lOGO_pictureBox;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label STD_No_Label;
        private System.Windows.Forms.TextBox STD_NO_TextBox;
        private System.Windows.Forms.Label Join_Date_Label;
        private System.Windows.Forms.TextBox JoinDate_Textbox;
        private System.Windows.Forms.Label FeeInfo_label;
        private System.Windows.Forms.CheckBox Discount_checkBox;
        private System.Windows.Forms.Label Discount_Amount_Label;
        private System.Windows.Forms.TextBox Discount_Amount_textBox;
        private System.Windows.Forms.Label Paid_Amount_label;
        private System.Windows.Forms.TextBox Paid_Amount_textBox;
        private System.Windows.Forms.TextBox ToBePaid_textBox;
        private System.Windows.Forms.Label To_Be_Paid_label;
        private System.Windows.Forms.Label Course_Fee_label;
        private System.Windows.Forms.TextBox Course_Fee_textBox;
        private System.Windows.Forms.Button Save_button;
        private System.Windows.Forms.Button Back_button;
    }
}

