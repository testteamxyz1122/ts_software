﻿namespace TechStep
{
    partial class Status
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Status));
            this.btn_Cleared = new System.Windows.Forms.RadioButton();
            this.btn_UnClear = new System.Windows.Forms.RadioButton();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btn_Check = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lOGO_pictureBox = new System.Windows.Forms.PictureBox();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lOGO_pictureBox)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_Cleared
            // 
            this.btn_Cleared.AutoSize = true;
            this.btn_Cleared.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Cleared.ForeColor = System.Drawing.Color.Maroon;
            this.btn_Cleared.Location = new System.Drawing.Point(520, 260);
            this.btn_Cleared.Name = "btn_Cleared";
            this.btn_Cleared.Size = new System.Drawing.Size(94, 28);
            this.btn_Cleared.TabIndex = 31;
            this.btn_Cleared.TabStop = true;
            this.btn_Cleared.Text = "Cleared";
            this.btn_Cleared.UseVisualStyleBackColor = true;
            // 
            // btn_UnClear
            // 
            this.btn_UnClear.AutoSize = true;
            this.btn_UnClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_UnClear.ForeColor = System.Drawing.Color.Maroon;
            this.btn_UnClear.Location = new System.Drawing.Point(648, 260);
            this.btn_UnClear.Name = "btn_UnClear";
            this.btn_UnClear.Size = new System.Drawing.Size(164, 28);
            this.btn_UnClear.TabIndex = 32;
            this.btn_UnClear.TabStop = true;
            this.btn_UnClear.Text = "Uncleared Dues";
            this.btn_UnClear.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(314, 353);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(769, 345);
            this.dataGridView1.TabIndex = 33;
            // 
            // btn_Check
            // 
            this.btn_Check.BackColor = System.Drawing.Color.Firebrick;
            this.btn_Check.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Check.ForeColor = System.Drawing.Color.White;
            this.btn_Check.Location = new System.Drawing.Point(851, 295);
            this.btn_Check.Name = "btn_Check";
            this.btn_Check.Size = new System.Drawing.Size(75, 38);
            this.btn_Check.TabIndex = 34;
            this.btn_Check.Text = "Check";
            this.btn_Check.UseVisualStyleBackColor = false;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Times New Roman", 21.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Firebrick;
            this.label16.Location = new System.Drawing.Point(463, 9);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(443, 33);
            this.label16.TabIndex = 61;
            this.label16.Text = "Get Knowledge , Spread Knowledge";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Modern No. 20", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(441, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(183, 34);
            this.label11.TabIndex = 30;
            this.label11.Text = "Check Status";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Firebrick;
            this.panel5.Controls.Add(this.label14);
            this.panel5.Location = new System.Drawing.Point(209, 45);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1147, 128);
            this.panel5.TabIndex = 75;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Times New Roman", 80.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label14.Location = new System.Drawing.Point(233, 3);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(601, 119);
            this.label14.TabIndex = 0;
            this.label14.Text = "TECHSTEP";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Times New Roman", 24F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label15.Location = new System.Drawing.Point(131, 137);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(72, 36);
            this.label15.TabIndex = 84;
            this.label15.Text = "Step";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 24F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label1.Location = new System.Drawing.Point(61, 137);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 36);
            this.label1.TabIndex = 83;
            this.label1.Text = "Tech";
            // 
            // lOGO_pictureBox
            // 
            this.lOGO_pictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("lOGO_pictureBox.BackgroundImage")));
            this.lOGO_pictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.lOGO_pictureBox.Location = new System.Drawing.Point(66, 48);
            this.lOGO_pictureBox.Name = "lOGO_pictureBox";
            this.lOGO_pictureBox.Size = new System.Drawing.Size(113, 89);
            this.lOGO_pictureBox.TabIndex = 82;
            this.lOGO_pictureBox.TabStop = false;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.Firebrick;
            this.panel9.Location = new System.Drawing.Point(1111, 179);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(245, 48);
            this.panel9.TabIndex = 77;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.Firebrick;
            this.panel8.Location = new System.Drawing.Point(1092, 179);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(13, 48);
            this.panel8.TabIndex = 81;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.Firebrick;
            this.panel7.Location = new System.Drawing.Point(1070, 179);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(13, 48);
            this.panel7.TabIndex = 80;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Firebrick;
            this.panel6.Location = new System.Drawing.Point(145, 180);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(18, 48);
            this.panel6.TabIndex = 79;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Firebrick;
            this.panel4.Location = new System.Drawing.Point(127, 180);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(12, 48);
            this.panel4.TabIndex = 78;
            this.panel4.Paint += new System.Windows.Forms.PaintEventHandler(this.panel4_Paint);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Firebrick;
            this.panel3.Location = new System.Drawing.Point(-65, 180);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(186, 48);
            this.panel3.TabIndex = 76;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Firebrick;
            this.panel2.Controls.Add(this.label11);
            this.panel2.Location = new System.Drawing.Point(169, 179);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(894, 47);
            this.panel2.TabIndex = 74;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Firebrick;
            this.panel1.Location = new System.Drawing.Point(-29, 277);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(27, 141);
            this.panel1.TabIndex = 73;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.Firebrick;
            this.panel10.Location = new System.Drawing.Point(12, 35);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(27, 141);
            this.panel10.TabIndex = 85;
            // 
            // Status
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(1354, 749);
            this.Controls.Add(this.panel10);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.btn_Check);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lOGO_pictureBox);
            this.Controls.Add(this.panel9);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.btn_UnClear);
            this.Controls.Add(this.btn_Cleared);
            this.Name = "Status";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Status";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Status_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lOGO_pictureBox)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.RadioButton btn_Cleared;
        private System.Windows.Forms.RadioButton btn_UnClear;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btn_Check;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox lOGO_pictureBox;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel10;
    }
}